#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
#

import sys

import gi
gi.require_version('Xfconf', '0')
from gi.repository import Xfconf

from application import ViCardApplication


def main(args):
    channel = None
    status = 0

    if Xfconf.init():
        channel = Xfconf.Channel.get('vicard')

        app = ViCardApplication(channel)
        status = app.run(args)
    else:
        print('Unable to connect to Xfconf daemon')

    sys.exit(status)


if __name__ == '__main__':
    main(sys.argv)
