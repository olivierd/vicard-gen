# SPDX-License-Identifier: 0BSD
#

import pathlib
import re
import unicodedata

import vobject
import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gtk



def utils_set_widget_image(name, size):
    image = None
    theme = Gtk.IconTheme.get_default()

    info = theme.lookup_icon(name, size,
                             Gtk.IconLookupFlags.FORCE_REGULAR)
    if info is not None:
        pixbuf = info.load_icon()
        image = Gtk.Image.new_from_pixbuf(pixbuf)

    return image

def utils_slugify_name(name, sep=b'-'):
    res = []
    pattern = re.compile(r'[\t !"#$%:&\'()*\-/<=>?@\[\\\]^_`{|},.]+')

    for c in pattern.split(name.lower()):
        c = unicodedata.normalize('NFKD', c).encode('ascii', 'ignore')
        if c:
            res.append(c)

    return sep.join(res)

def utils_pathlib_ob(path):
    '''Return pathlib.Path object.'''
    default_value = GLib.get_home_dir()

    # URI?
    if '://' in path:
        if path.startswith('file://'):
            g = Gio.File.new_for_uri(path)
            p = pathlib.Path(g.get_path())
        else:
            p = pathlib.Path(default_value)
    else:
        if path.startswith('~'):
            p = pathlib.Path(path).expanduser()
        else:
            p = pathlib.Path(path).resolve()

    return p

def utils_build_filename(dirname, name):
    p = pathlib.Path(dirname)
    slug = utils_slugify_name(name).decode('utf-8')

    n = p / slug
    filename = n.with_suffix('.vcf')

    return filename

def utils_create_card(data):
    '''Create vCard object.'''
    c = None

    if isinstance(data, dict):
        c = vobject.vCard()

        # Identity
        if 'first_name' and 'last_name' in data:
            c.add('n')
            c.n.value = vobject.vcard.Name(family=data.get('last_name'),
                                           given=data.get('first_name'))

            c.add('fn')
            c.fn.value = '{0} {1}'.format(data.get('first_name'),
                                          data.get('last_name'))

        # Address
        if 'city' and 'code' and 'street' in data:
            c.add('adr')
            if 'extended' in data:
                ext_adr = data.get('extended')
            else:
                ext_adr = ''

            c.adr.value = vobject.vcard.Address(street=data.get('street'),
                                                city=data.get('city'),
                                                code=data.get('code'),
                                                extended=ext_adr)

        # Email
        if 'email' in data:
            c.add('email')
            c.email.value = data.get('email')
            c.email.type_param = 'INTERNET'

        # Gender
        if 'gender' in data:
            c.add('gender')
            c.gender.value = data.get('gender')

    return c

def utils_write_card(root_dir, data):
    res = 0
    msg = None

    card = utils_create_card(data)

    if card is not None:
        if 'first_name' and 'last_name' in data:
            n = '{0} {1}'.format(data.get('first_name'),
                                 data.get('last_name'))

            # We have pathlib.Path object
            filename = utils_build_filename(root_dir, n)

            if filename.exists():
                res = -1
                msg = 'File already exists'
            else:
                filename.write_text(card.serialize(), encoding='UTF-8')
        else:
            res = -2
            msg = 'Internal error'
    else:
        res = -3
        msg = 'Not able to create vCard'

    return (res, msg)

def utils_populate_dict(data, pos, value):
    if pos == 0:
       data['first_name'] = value.title()
    elif pos == 1:
       data['last_name'] = value.title()
    elif pos == 2:
       data['city'] = value.title()
    elif pos == 3:
       data['code'] = value
    elif pos == 4:
       data['street'] = value.title()
    elif pos == 5:
       data['email'] = value.lower()

def get_root_directory():
    p = pathlib.Path(__file__).resolve()

    #
    root_dir = p.parent.parent

    return root_dir
