# SPDX-License-Identifier: 0BSD
#

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from utils import utils_set_widget_image


class ViCardDialog(Gtk.MessageDialog):
    def __init__(self, parent, msg):
        Gtk.MessageDialog.__init__(self, parent=parent,
                                   message_type=Gtk.MessageType.ERROR,
                                   use_header_bar=False,
                                   title='Error')

        self.set_icon_name('user-info')
        self.set_default_size(250, 100)
        self.add_button('Ok', Gtk.ResponseType.OK)

        box = self.get_message_area()
        box.set_orientation(Gtk.Orientation.VERTICAL)

        child_widget = self.dialog_populate_box(msg)
        box.pack_start(child_widget, True, True, 0)

        self.show_all()

        self.connect('response', self.dialog_response_cb)

    def dialog_populate_box(self, msg):
        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 6)

        label = Gtk.Label.new(msg)
        image = utils_set_widget_image('dialog-error',
                                       Gtk.IconSize.DIALOG)
        if image is not None:
            hbox.pack_start(image, False, False, 0)
            hbox.pack_end(label, True, True, 0)
        else:
            hbox.add(label)

        return hbox

    def dialog_response_cb(self, widget, response_id):
        if response_id == Gtk.ResponseType.OK:
            widget.destroy()
