# SPDX-License-Identifier: 0BSD
#

import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gtk

from window import ViCardWindow
from settings import ViCardSettings


class ViCardApplication(Gtk.Application):
    def __init__(self, channel):
        name_id = 'org.framagit.olivierd.vicard-gen'

        Gtk.Application.__init__(self,
                                 application_id=name_id,
                                 flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE)

        self.channel = channel

        # Optional option
        self.add_main_option('directory', ord('d'),
                             GLib.OptionFlags.NONE,
                             GLib.OptionArg.STRING,
                             'Folder where to store vCard', None)

    # Callback
    def app_quit_cb(self, action, param):
        self.quit()

    # Virtual methods
    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Ctrl-q shortcut
        action = Gio.SimpleAction.new('quit', None)
        action.connect('activate', self.app_quit_cb)
        self.add_action(action)
        self.set_accels_for_action('app.quit', ['<Control>q'])

        GLib.set_application_name('vCard generator')
        GLib.set_prgname('vcard_generator')

    def do_command_line(self, cmd):
        opts = cmd.get_options_dict()
        opts = opts.end().unpack()

        if 'directory' in opts:
            settings = ViCardSettings(self.channel)
            settings.set_save_directory(opts.get('directory'))

        self.activate()
        return 0

    def do_activate(self):
        window = ViCardWindow(self, self.channel)
        window.show_all()
