# SPDX-License-Identifier: 0BSD
#

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
from gi.repository import GLib, GObject, Gtk, Gdk

from dialog import ViCardDialog
from infobar import ViCardInfoBar
from settings import ViCardSettings
from utils import utils_populate_dict, utils_write_card


class ViCardWindow(Gtk.ApplicationWindow):
    def __init__(self, app, channel):
        Gtk.ApplicationWindow.__init__(self,
                                       application=app,
                                       show_menubar=False)

        self.set_icon_name('user-info')
        self.set_title('vCard Generator')

        self.settings = ViCardSettings(channel)
        self.data = {}
        self.infobar = None

        self.main_window()

    def win_get_content(self, entry):
        buf = entry.get_buffer()
        if buf.get_length() > 0:
            return buf.get_text()
        else:
            return None

    def win_clear_entries(self, entries):
        for entry in iter(entries):
            buf = entry.get_buffer()
            if buf.get_length() > 0:
                buf.delete_text(0, -1)

    def win_check_entries(self, entries):
        err = 0

        for i, entry in enumerate(entries):
            result = self.win_get_content(entry)
            if result is not None:
                value = result.strip()

                utils_populate_dict(self.data, i, value)
                entry.set_text(value)
            else:
                if i < 5:
                    err += 1

        return err

    def win_show_error(self, msg):
        if self.infobar.get_revealed():
            self.infobar.set_revealed(False)

        ViCardDialog(self, msg)

    def win_button_clicked_cb(self, button, user_data):
        err = self.win_check_entries(user_data)

        if err == 0:
            save_dir = self.settings.get_save_directory()

            res, message = utils_write_card(save_dir, self.data)
            if res < 0:
                self.win_show_error(message)
            elif res == 0:
                if not self.infobar.get_revealed():
                    self.infobar.set_revealed(True)

                self.win_clear_entries(user_data)
        else:
            self.win_show_error('Some fields are required')

    def win_configure_event_cb(self, widget, event):
        w = event.width
        h = event.height

        if w >= self.settings.get_window_width():
            self.settings.set_window_width(w)

        if h >= self.settings.get_window_height():
            self.settings.set_window_height(h)

    def win_state_event_cb(self, widget, event):
        if event.changed_mask == Gdk.WindowState.MAXIMIZED:
            if self.settings.is_window_fullscreen():
                # Default size of window
                width = self.settings.get_property('prop-width')
                height = self.settings.get_property('prop-height')

                self.settings.set_window_fullscreen(False)
                self.unmaximize()

                # Resize and update properties
                self.resize(width, height)
                self.set_window_size(width, height)
            else:
                self.settings.set_window_fullscreen(True)
                self.maximize()

    def win_scrollbars_realize(self, hscroll=False):
        '''Initialize Gtk.ScrolledWindow widget.'''
        widget = Gtk.ScrolledWindow.new(None, None)

        # Horizontal scrollbar
        if hscroll:
            policy = Gtk.PolicyType.AUTOMATIC
        else:
            policy = Gtk.PolicyType.NEVER

        widget.set_policy(policy,
                          Gtk.PolicyType.AUTOMATIC)

        return widget

    def win_make_entry(self, parent, text_label, col_nb, row_nb, width=1):
        '''Associate Gtk.Label with Gtk.Entry widgets.'''
        label = Gtk.Label.new(text_label)
        entry = Gtk.Entry.new()

        parent.attach(label, col_nb, row_nb, 1, 1)
        parent.attach_next_to(entry, label,
                              Gtk.PositionType.RIGHT, width, 1)

        return entry

    def win_populate_grid(self):
        entries = []

        grid = Gtk.Grid.new()
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        grid.props.margin = 4

        self.infobar = ViCardInfoBar('Add successfully')
        self.infobar.set_revealed(False)
        grid.attach(self.infobar, 0, 0, 4, 1)

        # First name widgets
        first_name = self.win_make_entry(grid, 'First name:', 0, 1)
        entries.append(first_name)

        # Last name widgets
        last_name = self.win_make_entry(grid, 'Last name:', 2, 1)
        entries.append(last_name)

        # Address widgets
        city = self.win_make_entry(grid, 'City:', 0, 2)
        entries.append(city)

        zip_code = self.win_make_entry(grid, 'Zip code:', 2, 2)
        entries.append(zip_code)

        street = self.win_make_entry(grid, 'Street:', 0, 3, width=3)
        entries.append(street)

        # Separator widget
        grid.attach(Gtk.Separator.new(Gtk.Orientation.HORIZONTAL),
                    0, 4, 4, 1)

        # Email widgets
        email = self.win_make_entry(grid, 'Email:', 0, 5, width=2)
        entries.append(email)

        apply_button = Gtk.Button.new_with_label('Save')
        grid.attach(apply_button, 3, 6, 1, 1)

        # Signal
        apply_button.connect('clicked', self.win_button_clicked_cb,
                             entries)

        return grid

    def main_window(self):
        if self.settings.is_window_fullscreen():
            self.maximize()
        else:
            self.props.default_width = self.settings.get_window_width()
            self.props.default_height = self.settings.get_window_height()

        # Implement scrollability for Gtk.Grid widget
        viewport = Gtk.Viewport.new(None, None)
        viewport.set_shadow_type(Gtk.ShadowType.NONE)
        viewport.add(self.win_populate_grid())

        # Allow scrolled main widget
        scrolled_win = self.win_scrollbars_realize(True)
        scrolled_win.add(viewport)
        self.add(scrolled_win)

        # Signals
        self.connect('configure_event', self.win_configure_event_cb)
        self.connect('window_state_event', self.win_state_event_cb)
