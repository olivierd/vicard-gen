# SPDX-License-Identifier: 0BSD
#
# Part of viCard-gen
#

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from utils import utils_set_widget_image


class ViCardInfoBar(Gtk.InfoBar):
    def __init__(self, msg):
        Gtk.InfoBar.__init__(self,
                             message_type=Gtk.MessageType.INFO,
                             show_close_button=True)

        box = self.get_content_area()
        box.set_orientation(Gtk.Orientation.HORIZONTAL)

        child_widget = self.infobar_populate_box(msg)
        box.pack_start(child_widget, True, True, 0)

        self.connect('response', self.infobar_response_cb)

    def infobar_populate_box(self, msg):
        hbox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 2)

        label = Gtk.Label.new(msg)
        img = utils_set_widget_image('dialog-information',
                                     Gtk.IconSize.BUTTON)

        if img is not None:
            hbox.pack_start(img, False, False, 0)
            hbox.pack_end(label, True, True, 0)
        else:
            hbox.add(label)

        return hbox

    def infobar_response_cb(self, widget, response_id):
        if response_id == Gtk.ResponseType.CLOSE:
            widget.set_revealed(False)
