# SPDX-License-Identifier: 0BSD
#

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Gio', '2.0')
gi.require_version('Xfconf', '0')
from gi.repository import GLib, GObject, Gio, Xfconf

from utils import utils_pathlib_ob


class ViCardSettings(GObject.Object):
    # Properties
    prop_width = GObject.Property(type=GObject.TYPE_INT,
                                  default=530,
                                  flags=GObject.ParamFlags.READABLE)
    prop_height = GObject.Property(type=GObject.TYPE_INT,
                                   default=250,
                                   flags=GObject.ParamFlags.READABLE)

    def __init__(self, channel):
        GObject.Object.__init__(self)

        self.channel = channel

    def get_window_width(self):
        # Default value
        default_value = self.get_property('prop-width')
        res = default_value

        if self.channel.has_property('/window/width'):
            res = self.channel.get_int('/window/width', default_value)

        return res

    def set_window_width(self, value):
        self.channel.set_int('/window/width', value)

    def get_window_height(self):
        # Default value
        default_value = self.get_property('prop-height')
        res = default_value

        if self.channel.has_property('/window/height'):
            res = self.channel.get_int('/window/height', default_value)

        return res

    def set_window_height(self, value):
        self.channel.set_int('/window/height', value)

    def is_window_fullscreen(self):
        res = False

        if self.channel.has_property('/window/fullscreen'):
            res = self.channel.get_bool('/window/fullscreen', False)

        return res

    def set_window_fullscreen(self, value):
        self.channel.set_bool('/window/fullscreen', value)

    def get_save_directory(self):
        # Default value
        default_value = GLib.get_home_dir()
        res = default_value

        if self.channel.has_property('/save/directory'):
            res = self.channel.get_string('/save/directory',
                                          default_value)

        return res

    def set_save_directory(self, value):
        p = utils_pathlib_ob(value)

        if not p.exists():
            p.mkdir(parents=True)

        self.channel.set_string('/save/directory', '{0}'.format(p))
