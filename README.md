# vCard Generator

This is a simple and lightweight Python application, in order to create
Virtual Contact File (VCF also known as vCard).

![Screenshot of vCard generator application](screenshot.png)

## Requirements

- Python3
- [PyGObject](https://gitlab.gnome.org/GNOME/pygobject)
- [VObject](http://eventable.github.io/vobject/)

### RPM-based

- glib2-devel
- gtk3-devel
- python3-gobject
- xfconf-devel >= 4.13.8 (support of GObject introspection)

### Debian and Ubuntu-like

**Not supported, because of lack of Xfconf gir bindings!**

### FreeBSD

- glib
- gtk3
- py36-gobject3
- xfconf

## How to use it?

Run `python3 src/main.py`

If every thing is fine, vCard is saved into `$HOME`. You can override
this setting:

    xfconf-query -c vicard -p /save/directory -n -t string -s ~/Desktop

Or

    python3 src/main.py -d ~/Desktop
